import React, { useState, useEffect } from 'react';
import { View,Text,TextInput } from 'react-native';

import styles from './styles';
import PageHeader from '../../components/PageHeader';
import TeacherItem, { Teacher } from '../../components/TeacherItem';
import { ScrollView, BorderlessButton, RectButton } from 'react-native-gesture-handler';
import { Feather } from '@expo/vector-icons'
import api from '../../services/api';
import AsyncStorage from '@react-native-community/async-storage';
import { useFocusEffect } from '@react-navigation/native';

const TeacherList: React.FC = () => {

  const [isFiltersVisible , setIsFiltersVisible] = useState(false);

  const [ subject, setSubject ] = useState('');
  const [ week_day, setWeekDay ] = useState('');
  const [ time, SetTime ] = useState('');

  const [teachers,setTeachers] = useState([]);
  const [ favorites, setFavorites ] = useState<number[]>([]);


  function loadFavorites() {
    AsyncStorage.getItem('favorites').then(response => {
      if( response  ){
        const favoritedTeachers = JSON.parse(response);
        const favoritedTeachersId = favoritedTeachers.map( (teacher:Teacher) => {
          return teacher.id;
        })

        setFavorites(favoritedTeachersId);
      }
    });
  }

  // useEffect( () => {
  //   loadFavorites();
  // },[])

  useFocusEffect( () => {
    loadFavorites();
  })


  function handleToogleFilterVisible() {
    setIsFiltersVisible(!isFiltersVisible);
  }

  async function handleFiltersSubmit() {
    loadFavorites();
    const  response =  await api.get('/classes' , {
      params:{
        subject,
        week_day,
        time
      }
    });

    setIsFiltersVisible(false);
    setTeachers(response.data);

  }

  return (
    <View style={styles.container}>
      <PageHeader 
      title="Proffys disponíveis"
      headerRight={(
        <BorderlessButton onPress={handleToogleFilterVisible}>
          <Feather name="filter" size={20} color="#FFF"></Feather>
        </BorderlessButton>
      )}>
        
       { isFiltersVisible &&  (
       
        <View style={styles.searchForm}>

          <Text style={styles.label}>Matéria</Text>
          <TextInput
          value={subject}
          onChangeText={ text => setSubject(text) }
          placeholderTextColor="#c1bccc"
          style={styles.input}
          placeholder="Qual a matéria?"
          ></TextInput>

          <View  style={styles.inputGroup}>
            <View style={styles.inputBlock}>
              <Text style={styles.label}>Dia da semana</Text>
              <TextInput
              value={week_day}
              onChangeText={ text => setWeekDay(text) }
              placeholderTextColor="#c1bccc"
              style={styles.input}
              placeholder="Qual o dia?"
              ></TextInput>
            </View>

            <View style={styles.inputBlock}>
              <Text style={styles.label}>Hoŕario</Text>
              <TextInput
              value={time}
              onChangeText={ text => SetTime(text) }
              placeholderTextColor="#c1bccc"
              style={styles.input}
              placeholder="Qual horário?"
              ></TextInput>
            </View>
          </View>

          <RectButton 
          onPress={handleFiltersSubmit}
          style={styles.submitButton}>
            <Text style={styles.submitButtonText}>Filtrar</Text>
          </RectButton>

        </View> 
       )}
       
      </PageHeader>

    <ScrollView
    style={styles.teacherList}
    contentContainerStyle={{
      paddingHorizontal: 16,
      paddingBottom: 16
    }}
    >


    { teachers.map( (teacher:Teacher) => {
      return (
        <TeacherItem 
          key={teacher.id} 
          teacher={teacher}
          favorited={favorites.includes(teacher.id)}
          ></TeacherItem>
      )
    })}
    
      
    </ScrollView>

    </View>
  )
}

export default TeacherList;